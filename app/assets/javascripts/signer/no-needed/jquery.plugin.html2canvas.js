/**
  @license html2canvas v0.34 <http://html2canvas.hertzen.com>
  Copyright (c) 2011 Niklas von Hertzen. All rights reserved.
  http://www.twitter.com/niklasvh

  Released under MIT License
 */
/*
 * jQuery helper plugin for examples and tests
 */
(function( $ ){
    $.fn.html2canvas = function(options) {
        if (options && options.profile && window.console && window.console.profile) {
            console.profile();
        }
        var $htmlmessage = '<h1>Form has been submitted - Please print or save for your records.</h1>';
        var date = new Date(),
        html2obj,
        $message = null,
        timeoutTimer = false,
        timer = date.getTime();
        options = options || {};

        options.onrendered = options.onrendered || function( canvas ) {
            var $canvas = $(canvas),
            finishTime = new Date();

            if (options && options.profile && window.console && window.console.profileEnd) {
                console.profileEnd();
            }
            $canvas.css({
                position: 'absolute',
                left: 0,
                top: 0
            }).appendTo('#quicksigndoc');
             $(this).find('canvas').attr('id', 'totalcanvas');
   var canvas2 = document.getElementById('totalcanvas');
   var dataURL2 = canvas2.toDataURL("image/png");

     $(this).html('<img src="'+ dataURL2 +'" />');
  //save to img
   function CanvasSaver(url) {

    this.url = url;

    this.savePNG = function(cnvs, fname) {
        if(!cnvs || !url) return;
        fname = fname || 'picture';

        var data = cnvs.toDataURL("image/png");
        data = data.substr(data.indexOf(',') + 1).toString();

        var dataInput = document.createElement("input") ;
        dataInput.setAttribute("name", 'imgdata') ;
        dataInput.setAttribute("value", data);


        var nameInput = document.createElement("input") ;
        nameInput.setAttribute("name", 'name') ;
        nameInput.setAttribute("value", fname + '.png');


         var fnameval = $("#fname").val();
         var fnameInput = document.createElement("input") ;
        fnameInput.setAttribute("name", 'fname') ;

        fnameInput.setAttribute("value", fnameval);

      
         var validateInput = document.createElement("input") ;
        validateInput.setAttribute("type", 'hidden') ;
        validateInput.setAttribute("name", 'imahuman');
        validateInput.setAttribute("id", 'imahuman');
        validateInput.setAttribute("value", '0');

         var mydateInput = document.createElement("input") ;
         mydateInput.setAttribute("name", 'thedate') ;
         var my_date=new Date();
         my_date.getDate();

          mydateInput.setAttribute("value", my_date);


        var myForm = document.createElement("form");
          myForm.setAttribute("id", 'saveform') ;
        myForm.method = 'post';
        myForm.action = url;

        myForm.appendChild(dataInput);
        myForm.appendChild(nameInput);
        //if save to disk
         if($('#qssave').hasClass('savetodisk')){
         myForm.appendChild(fnameInput);
          myForm.appendChild(mydateInput);
          };

        document.body.appendChild(myForm) ;
        myForm.submit() ;
        document.body.removeChild(myForm) ;
    };

    this.generateButton = function (label, cnvs, fname) {
        var btn = document.createElement('button'), scope = this;

        btn.innerHTML = label;
         btn.setAttribute('id', 'saveimg');
        btn.style['class'] = 'canvassaver';

        btn.addEventListener('click', function(){scope.savePNG(cnvs, fname);}, false);


        document.getElementById('quicksigndoc').appendChild(btn);

        return btn;
    };
}



  var cs = new CanvasSaver('/documents/image_save/1');
cs.generateButton('save an image!', canvas2, 'QuickSignForm');
//end save to image
     /* $.post(
    "quicksign/PHPMailer/save.php",     // url
    { imgdata : dataURL2 },         // post-data
    function(data){                                // response
        console.log(data);
    }
);     */
  $('#saveimg').trigger('click');
  $('#quicksigndoc').html($htmlmessage+'<img src="'+dataURL2+'" />');
  $('#savefileform').html('<h3>Thank You</h3>');
  $('#savefileform').delay(1000).fadeOut(2000);
            $canvas.siblings().toggle();

            //$(window).click(function(){
             //   $canvas.toggle().siblings().toggle();
             //   throwMessage("Canvas Render " + ($canvas.is(':visible') ? "visible" : "hidden"));
           // });

           $('#quicksignwrap').hide();
           // var canvas2 = document.getElementById('totalcanvas');
            //var dataURL2 = canvas2.toDataURL("image/png");
            throwMessage('Screenshot created in '+ ((finishTime.getTime()-timer)) + " ms<br />",4000);

            // test if canvas is read-able
            try {
                $canvas[0].toDataURL();
            } catch(e) {
                if ($canvas[0].nodeName.toLowerCase() === "canvas") {
                    // TODO, maybe add a bit less offensive way to present this, but still something that can easily be noticed
                    alert("Canvas is tainted, unable to read data");
                }
            }
        };

        html2obj = html2canvas(this, options);

        function throwMessage(msg,duration){
            window.clearTimeout(timeoutTimer);
            timeoutTimer = window.setTimeout(function(){
                $message.fadeOut(function(){
                    $message.remove();
                    $message = null;
                });
            },duration || 2000);
            if ($message)
                $message.remove();
            $message = $('<div />').html(msg).css({
                margin:0,
                padding:10,
                background: "#000",
                opacity:0.7,
                position:"fixed",
                top:10,
                right:10,
                fontFamily: 'Tahoma',
                color:'#fff',
                fontSize:12,
                borderRadius:12,
                width:'auto',
                height:'auto',
                textAlign:'center',
                textDecoration:'none',
                display:'none'
            }).appendTo(document.body).fadeIn();
            html2obj.log(msg);
        }
    };
})( jQuery );

