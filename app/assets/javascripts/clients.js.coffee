# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

#= require_directory ./signer

next_tab = ->
  active = $('.document-tab.menu').find('a.active')
  sibling = active.next('a')
  client_id = $('.document-tab.menu').data('client');
  sibling_tab_name = sibling.data('tab')
  $('.document-tab.menu .item').tab 'deactivate all'
                               .tab 'activate tab', sibling_tab_name
                               .tab 'activate navigation', sibling_tab_name

  active_step = $('.ui.vertical.steps').find('.step.active')
  active_step.removeClass('active')
  active_step.next('.step').addClass("active")
  if(sibling_tab_name == 'finished')
    create_docs(sibling,client_id)
  return

create_docs = (container,client_id)->
  $.get '/clients/'+client_id+'/prepare_documents', (data) ->
    $('.download-docs').html(data)


$(document).ready ->
  $(".document-tab.menu .item").tab()
  $('.ui.rating').rating()

  $(".next-button").click ->
    next_tab()
    return


  $('.document-sign-form').submit (e)->
    e.preventDefault()
    self = $(this)
    data_name = self.data('name')
    document_id = self.data('docid')
    image = self.find('img[data-name="'+data_name+'"]').attr('src')
    if(image == '/assets/qsback.png')
      self.find('.quicksign').popup("show");
      return
    $.ajax '/documents/'+document_id+'/sign.json',
      type: "POST",
      beforeSend: (xhr) -> xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
      dataType: 'json'
      data:
          image_data: image
      error: (jqXHR, textStatus, errorThrown) ->
        console.log "#{textStatus}"
      success: (data, textStatus, jqXHR) ->
        next_tab()

        #TODO mark as saved

  return






