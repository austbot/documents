class Document < ActiveRecord::Base
  belongs_to :client

  validates_presence_of :title
  markdownize! :document

  def has_client?
    return self.client
  end
end
