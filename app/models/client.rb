class Client < ActiveRecord::Base
  has_many :documents
  validates_presence_of :name
  validates_presence_of :email
  validates_presence_of :phone
  validates :email, confirmation: true
  validates :email, format: { with: /\A[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]+\z/,
                                    message: "Must be email user @ domain . tld" }
  has_many :acess_codes

  def avatar_url()
    default_url = "/assets/guest.png"
  end
end
