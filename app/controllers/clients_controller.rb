class ClientsController < ApplicationController
  before_action :set_client, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user! , only: [:index,:edit,:update,:destroy,:new,:create,:codes,:access,:access_create]
  before_action :auth_code, only: [:show,:prepare_docs]
  # GET /clients
  # GET /clients.json
  def index
    @clients = Client.all
  end

  # GET /clients/1
  # GET /clients/1.json
  def show
    @client = Client.find(params[:id])
    @documents = @client.documents
  end

  # GET /clients/new
  def new
    @client = Client.new
  end

  # GET /clients/1/edit
  def edit
  end

  #GET /clients/access
  def access
    @clients = Client.all
    @access = AcessCode.new
  end

  #GET /client/1/:code
  def auth
    if params[:id] and params[:access_code]
      @access = AcessCode.find_by(code: params[:access_code])
      @client = Client.find(params[:id])
      if @access and @client and @client.id == @access.client.id
        session[:access_code] = @access.code
        redirect_to client_path, :id => @client.id
      else
        redirect_to "https://www.google.com"
      end
    else
      redirect_to "https://www.google.com"
    end
  end

  def access_create
    @access = AcessCode.new
    @access.code=SecureRandom.uuid
    @access.expiry = 10.days.from_now
    @access.client_id = params[:acess_code][:client_id]
    @access.save
    redirect_to clients_codes_path
  end

  def codes
    @clients = Client.all
    @access = AcessCode.all
  end

  #GET /clients/1/prepare_docs
  def prepare_docs
    @client = Client.find(params[:id])
    @documents = @client.documents
    @documents.each do |document|
      @document = document
      pdf = WickedPdf.new.pdf_from_string(render_to_string('documents/document', :layout => 'pdf'))
      url = "/pdfs/document-#{@document.id}.pdf"
      save_path = "public/#{url}"
      File.open(save_path, 'wb') do |file|
        file << pdf
      end
      @document.file_url = url
      @document.save
    end
    render 'documents/items',:layout => false
  end

  # POST /clients
  # POST /clients.json
  def create
    @client = Client.new(client_params)

    respond_to do |format|
      if @client.save
        format.html { redirect_to @client, notice: 'Client was successfully created.' }
        format.json { render action: 'show', status: :created, location: @client }
      else
        format.html { render action: 'new' }
        format.json { render json: @client.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /clients/1
  # PATCH/PUT /clients/1.json
  def update
    respond_to do |format|
      if @client.update(client_params)
        format.html { redirect_to @client, notice: 'Client was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @client.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /clients/1
  # DELETE /clients/1.json
  def destroy
    @client.destroy
    respond_to do |format|
      format.html { redirect_to clients_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_client
      @client = Client.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def client_params
      params.require(:client).permit(:name, :email,:email_confirmation,:phone)
    end

    def auth_code
      if session[:access_code]
        @access = AcessCode.find_by(code: session[:access_code])
        if @access.client.id == params[:id].to_i
          return true
        end
      else
        redirect_to "https://www.google.com"
      end
    end
end
