class DocumentsController < ApplicationController
  before_action :set_document, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!, only: [:index,:new,:create,:update,:destroy]
  # GET /documents
  # GET /documents.json
  def index
    @documents = Document.all
  end

  # GET /documents/1
  # GET /documents/1.json
  def show
  end

  # GET /documents/new
  def new
    @document = Document.new
  end

  # GET /documents/1/edit
  def edit
  end

  # POST /documents/1/sign.json
  def sign
    @document = Document.find(params[:id])
    if(@document)
      image = params[:image_data]
      decoded_file = Base64.decode64(image['data:image/png;base64,'.length .. -1])
      path ="public/signs/document-#{@document.id}.png"
      begin
        File.open(path, 'wb') do|f|
          f.write(decoded_file)
        end
      rescue Exception => e
        render :json => e.message.as_json
        return
      end
      @document.seen_from_ip = request.remote_ip
      @document.client_seen = true
      @document.signed_or_accepted = true
      if @document.save()
        render :json => {:image => path.as_json}
      end
    else
      render :json => "Doc Not Found".as_json
    end
  end

  # POST /documents
  # POST /documents.json
  def create
    @document = Document.new(document_params)

    respond_to do |format|
      if @document.save
        format.html { redirect_to @document, notice: 'Document was successfully created.' }
        format.json { render action: 'show', status: :created, location: @document }
      else
        format.html { render action: 'new' }
        format.json { render json: @document.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /documents/1
  # PATCH/PUT /documents/1.json
  def update
    respond_to do |format|
      if @document.update(document_params)
        format.html { redirect_to @document, notice: 'Document was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @document.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /documents/1
  # DELETE /documents/1.json
  def destroy
    @document.destroy
    respond_to do |format|
      format.html { redirect_to documents_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_document
      @document = Document.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def document_params
      params.require(:document).permit(:title, :client_id,:document)
    end
end
