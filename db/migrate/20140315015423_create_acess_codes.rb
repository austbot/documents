class CreateAcessCodes < ActiveRecord::Migration
  def change
    create_table :acess_codes do |t|
      t.string :code
      t.datetime :expiry
      t.timestamps
    end
  end
end
