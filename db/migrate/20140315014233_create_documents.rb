class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.string :title , null: false
      t.string :file_url, null: true
      t.boolean :signed_or_accepted, null: false, default: false
      t.boolean :client_seen, null:false, default: false
      t.string :seen_from_ip, null: true
      t.text :document
      t.belongs_to :client
      t.timestamps
    end
  end
end
