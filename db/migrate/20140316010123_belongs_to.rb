class BelongsTo < ActiveRecord::Migration
  def change

    drop_table :acess_codes
    create_table :acess_codes do |t|
      t.string :code
      t.datetime :expiry
      t.belongs_to :client
      t.timestamps
    end
  end
end
