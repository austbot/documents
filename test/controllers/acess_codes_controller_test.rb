require 'test_helper'

class AcessCodesControllerTest < ActionController::TestCase
  setup do
    @acess_code = acess_codes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:acess_codes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create acess_code" do
    assert_difference('AcessCode.count') do
      post :create, acess_code: { code: @acess_code.code, expiry: @acess_code.expiry }
    end

    assert_redirected_to acess_code_path(assigns(:acess_code))
  end

  test "should show acess_code" do
    get :show, id: @acess_code
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @acess_code
    assert_response :success
  end

  test "should update acess_code" do
    patch :update, id: @acess_code, acess_code: { code: @acess_code.code, expiry: @acess_code.expiry }
    assert_redirected_to acess_code_path(assigns(:acess_code))
  end

  test "should destroy acess_code" do
    assert_difference('AcessCode.count', -1) do
      delete :destroy, id: @acess_code
    end

    assert_redirected_to acess_codes_path
  end
end
