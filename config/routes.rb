Documents::Application.routes.draw do
    root "documents#index"
    get "access/:acess_code" => "acess_codes#index"
    if Rails.env.production?
      devise_for :users, :controllers => { :registrations => "registrations" }
    else
      devise_for :users
    end
    resources :documents
    post "documents/:id/sign" => "documents#sign"
    get 'clients/:id/prepare_documents' => 'clients#prepare_docs'
    get 'clients/:id/:access_code' => "clients#auth"
    post 'clients/access' => "clients#access_create"
    get 'clients/access' => "clients#access"
    get 'clients/codes' => "clients#codes"
    resources :clients
end
